package facena.tais.testing.banco;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import facena.tais.testing.banco.Cuenta;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pablo N. Garcia Solanellas
 */
public class CuentaTest {
    
    public CuentaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    
    //IMPORTANTE:  Los metodos que corren las pruebas deben tenes la anotacion @Test    
    // Para correr TODOS los TEST [Alt + F6]
    // Para correr SOLO los TEST de esta clase [Ctrl + SHIFT + F6]    
    @Test
    public void testCta() {
        
        CuentaTest.constructorPorDefecto();
        CuentaTest.sinNumero();
        CuentaTest.sinFechaAlta();
        
    }
    
    
    /**
     * Probamos una instancia de la clase Cuenta creada por el constructor por defecto.
     */
    private static void constructorPorDefecto(){
        /*
        * Instanciamos una cuenta con el constructor por defecto.
        * -SIN Numero de cuenta. 
        * -SIN fecha de alta.        
         */
        Cuenta _cuenta = new Cuenta();
        
        /* 
        * Esperamos que el metodo isCuentaValida nos indique que esta no es valida.
        * ya que no cuenta ni con numero de cuenta ni con una fecha de alta.
        */        
        assertFalse(_cuenta.isCuentaValida());
    }
    
    /**
     * Probamos una instancia de la clase Cuenta que no tiene numero de cuenta
     */
    private static void sinNumero(){
        /* Instanciamos una cuenta INVALIDA. 
        * -SIN Numero de cuenta. 
        * -CON fecha de alta.
        */        
        Cuenta _cuentaInvalidaDos = new Cuenta(null,new Date(), 0, 0);
        
        //esperamos que el metodo isCuentaValida nos indique que esta cuenta
        // no es valida ya que no cuenta con un NUMERO de cuenta.
        assertFalse(_cuentaInvalidaDos.isCuentaValida());
    }
     
    /**
     * Probamos una instancia de la clase Cuenta que no tiene fecha de alta.
     */
    private static void sinFechaAlta(){
        /* Instanciamos una cuenta INVALIDA. 
        * -CON Numero de cuenta. 
        * -SIN fecha de alta.
        */        
        Cuenta _cuentaInvalidaTres = new Cuenta(456789,null, 0, 0);
        
        // esperamos que el metodo isCuentaValida nos indique que esta cuenta
        //no es valida ya que no cuenta con una fecha de alta.        
        assertFalse(_cuentaInvalidaTres.isCuentaValida());
    }
}
