/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facena.tais.testing.usuario;


import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pablo N. Garcia Solanellas
 */
public class UsuarioTest {
    
    public UsuarioTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    //IMPORTANTE:  Los metodos que corren las pruebas deben tenes la anotacion @Test    
    // Para correr TODOS los TEST [Alt + F6]
    // Para correr SOLO los TEST de esta clase [Ctrl + SHIFT + F6]    
    @Test
    public void pruebas() {
        
       
        //En esta coleccion vamos a guardar los objetos para realizar el test 
        //y los valores de fortaleza esperados.
        Map<Usuario, Byte> _scoring = new HashMap<Usuario,Byte>();
        
        //LOTE DE PRUEBAS
        //nombre de usaurio y contraseña iguales, menos de 7 caracteres, sin numero 0-1
        _scoring.put(new Usuario("pablo", "pablo") ,(byte) -1);        
        
        //nombre de usaurio y contraseña iguales, con numeros (1-1)       
        _scoring.put(new Usuario( "pablo1", "pablo1") ,(byte) 0);        
        
        //nombre de usaurio y contraseña iguales, menos de 7 caracteres, con numeros y mayusculas (2-1)
        _scoring.put(new Usuario( "Pablo1", "Pablo1") ,(byte) 1);        
        
        //nombre de usaurio y contraseña iguales, mas de 7 caracteres, con numeros y mayusculas (3-1)      
        _scoring.put(new Usuario( "Pablo", "Pablo123") ,(byte) 2);        
        
        //nombre de usaurio y contraseña iguales, menos de 7 caracteres, con numeros y mayusculas       
        _scoring.put(new Usuario( "Pablo", "Pablo123") ,(byte) 2);        
        
        //letras numeros y signos
        _scoring.put(new Usuario( "pablo", "?!p13") ,(byte) 2);
        
        //letras (mayusculas y minusculas), numeros y signos mas de 7 caracteres
        _scoring.put(new Usuario( "pablo", "??!pBL123") ,(byte) 4);
        
        //letras (mayusculas y minusculas), numeros y signos mas de 11 7 caracteres
        _scoring.put(new Usuario( "pablo", "??!paBL1y2uw3/") ,(byte) 5);         
        
        //run test
        for(Map.Entry<Usuario,Byte> _entry : _scoring.entrySet()){
            assertEquals (_entry.getValue(), (Byte) _entry.getKey().fortalezaDeLaContrazeña() );                 
            //System.err.println( "PWD: " + _entry.getKey().getContraseña() + " fortaleza "  + _entry.getKey().fortalezaDeLaContrazeña());
        }
        
        
    }
    
}
