/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facena.tais.testing.numeros;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pablo N. Garcia Solanellas
 */
public class HerramientaNumeroTest {
    
    public HerramientaNumeroTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:    
    @Test
    public void miTest() {
        //TDD
        int[] _arreglo ={0,2,1};
        assertEquals(2, HerramientaNumero.maximo( _arreglo ) );
        
        int[] _arreglo2 ={0,-2,-1};
        assertEquals(0, HerramientaNumero.maximo( _arreglo2 ) );
        
        int[] _arreglo3 ={Integer.MAX_VALUE,-2,-1};
        assertEquals(Integer.MAX_VALUE, HerramientaNumero.maximo( _arreglo3 ) );
    }
}
