/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facena.tais.testing.numeros;

/**
 *
 * @author Pablo N. Garcia Solanellas
 */
public class HerramientaNumero {
    
    public static int maximo(int[] p_array){
        int _nuestroMaximo =Integer.MIN_VALUE;
        int i = 0;
        for(i = 0; i<p_array.length; i++){
            if (p_array[i]>_nuestroMaximo ){
                _nuestroMaximo  = p_array[i];
            }            
        }
        
        return _nuestroMaximo;
        
    }
}
