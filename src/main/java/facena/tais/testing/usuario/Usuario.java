/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facena.tais.testing.usuario;

/**
 *
 * @author Pablo N. Garcia Solanellas
 */
public class Usuario {
    private String nombre;
    private String contraseña;
    
    public Usuario(){}
    
    public Usuario(String p_contraseña){
        this.setContraseña(p_contraseña);
    }
    public Usuario(String p_nombre, String p_contraseña){
        this.setNombre(p_nombre);
        this.setContraseña(p_contraseña);
    }
    
    public byte fortalezaDeLaContrazeña(){        
        byte _fortaleza=0;//scoring de la contraceña
        
        //contraseña contiene el nombre de usuario
        if (this.getContraseña().contains(this.getNombre()))
        {
            _fortaleza--;
        }

        //+1 más de 7 caracteres
        if (this.getContraseña().length()>7)
        {
            _fortaleza++;
        }
        
        //+1 más de 11 caracteres
        if (this.getContraseña().length()> 11 )
        {
            _fortaleza++;
        }
        
        //continence numeros        
        if(this.getContraseña().matches(".*[0-9].*")){
             _fortaleza++;
        }        
        //continence Mayusculas y minusculas
        if(this.getContraseña().matches(".*[a-z].*") 
                && this.getContraseña().matches(".*[A-Z].*") ){
             _fortaleza++;
        }
        
        //continence signos de puntuacion
        if(this.getContraseña().matches(".*[.!?\\-].*")){
             _fortaleza++;
        }  
        
        return _fortaleza;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }
        
    
            
}
